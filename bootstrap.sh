#!/bin/bash

# setting spark defaults
echo spark.yarn.jar hdfs://spark/spark-assembly-"${SPARK_VERSION}"-hadoop"${HADOOP_VERSION}".jar > "${SPARK_HOME}"/conf/spark-defaults.conf
cp "${SPARK_HOME}"/conf/metrics.properties.template "${SPARK_HOME}"/conf/metrics.properties

#Start Master and Worker
."${SPARK_HOME}"/sbin/start-master.sh
."${SPARK_HOME}"/sbin/start-slave.sh spark://"${SPARK_MASTER_IP}":"${SPARK_MASTER_PORT}" 

while :; do
 sleep 300
done